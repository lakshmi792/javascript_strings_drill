let ip = "111.139.161.143";

function problem2(ipaddress = ip) {
    if (ipaddress[0] == '.' || ipaddress[ipaddress.length - 1] == '.') {
        return [];
    }
    if (ipaddress.length == 0) {
        return [];
    }
    let answer = ipaddress.split('.');
    if (answer.length > 4 || answer.length < 4) {
        return [];
    }

    let values = [];
    for (let key of answer) {
        if (key.length > 3) {
            return [];
        }
        if (isNaN(Number(key))) {
            return [];
        }
        else {
            values.push(Number(key));
        }
    }
    return values;
}

let result = problem2(ip);

//console.log(result);

module.exports = problem2;
