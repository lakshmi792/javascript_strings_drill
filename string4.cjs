let obj = { "first_name": "JoHN", "middle_name": "doe", "last_name": "SMith" };

function problem4(object = obj) {
    if (object.length == 0) {
        return {};
    }
    if (typeof (object) != 'object') {
        return {};
    }
    let answer = "";
    for (let key in object) {
        if (typeof (object[key]) != 'string') {
            return {};
        }
        let value = object[key];
        answer = answer.concat(" ", (value[0].toUpperCase() + (value.slice(1, value.length)).toLowerCase()));
    }
    return answer;
}

let result = problem4(obj);
//console.log(result);

module.exports = problem4;
