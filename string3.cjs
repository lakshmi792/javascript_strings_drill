let dates = "12/9/2021";

function problem3(date = dates) {
    if (date.length == 0) {
        return [];
    }
    if (date[0] == '/' || date[date.length - 1] == '/') {
        return [];
    }
    let answer = date.split('/');
    if (answer.length != 3) {
        return [];
    }

    for (let key of answer) {
        if (isNaN(Number(key))) {
            return [];
        }
        if(Number(key)<1){
            return [];
        }
    }
    let mon = Number(answer[1]);
    let day = Number(answer[0]);

    if(day>31 || mon>12){
        return [];
    }
   

    let month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


    let value = `${month[mon - 1]} ${day}`;
    return value;
}
let result = problem3(dates);
//console.log(result);

module.exports = problem3;

