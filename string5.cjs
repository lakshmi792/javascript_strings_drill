let arrays = ["the", "quick", "brown", "fox"];


function problem5(array = arrays) {
    if (array.length == 0) {
        return [];
    }
    if (!Array.isArray(array)) {
        return [];
    }
    let answer = array.join(" ");
    let ans = String(answer);
    return ans;

}

let result = problem5(arrays);
//console.log(result);

module.exports = problem5;