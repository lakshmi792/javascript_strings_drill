let numbers = ["$100.45", "$1,002.22", "-$123"];

function problem1(number = numbers) {
    if (number.length == 0) {
        return 0;
    }
    if (!Array.isArray(number)) {
        return 0;
    }
    let answer = [];
    let chars = "";
    for (let value of number) {

        if (value[0] == '$') {
            chars = value.replace(value[0], '');
        }
        if (value[1] == '$') {
            chars = value.replace(value[1], '');
        }
        let removeComa = chars.replace(',', '');

        if (isNaN(Number(removeComa))) {
            return 0;
        }
        else {
            answer.push(Number(removeComa));
        }

    }
    return answer;
}

let result = problem1(numbers);
//console.log(result);

module.exports = problem1;

